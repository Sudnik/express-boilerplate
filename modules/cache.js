var errorLogger = require('../modules/consoleLogger').consoleErrorLogger;
var redisCacheConfig,
    config      = require('../config/config');

try {
    redisCacheConfig = require('../config/cacheParams.json');
} catch (e) {
    console.log('[' + (new Date()).toUTCString() + '] [cache] no redisCacheConfig file, using defaults');
    redisCacheConfig = {expires: 10};
}

if (config.disableExpressRedisCache) {
    module.exports = {
        route: () => (req, res, next) =>next()
    };
} else {
    module.exports = require('express-redis-cache')(redisCacheConfig)
        .on('error', function (e) {
            errorLogger(e.toString());
        });
}