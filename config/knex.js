"use strict";
var logger      = require('../modules/consoleLogger').consoleLogger,
    errorLogger = require('../modules/consoleLogger').consoleErrorLogger;

var dbConnParams;


try {
    dbConnParams = require('./dbConnectionParams.json') || {};
} catch(e) {
    errorLogger('no dbConnectionParams.json');
    dbConnParams = {};
}

module.exports = require('knex')({
    client    : 'mysql',
    connection: {
        host    : dbConnParams.host,
        user    : dbConnParams.user,
        password: dbConnParams.password,
        database: dbConnParams.database,
        timezone: dbConnParams.timezone
    },
    pool      : {
        requestTimeout: 5000,
        bailAfter     : Infinity,
        min           : 0,
        max           : dbConnParams.connectionLimit || 1
    }
});