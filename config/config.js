"use strict";

var configFile = './config.json';

try {
    var config = require(configFile);
} catch (e) {
    if (e.code === 'MODULE_NOT_FOUND') {
        console.error('no configuration file ' + configFile + ', using default configuration');
    } else {
        console.error(e);
    }
    config = {};
}

config.webServerParams = config.webServerParams || {};

module.exports = config;



