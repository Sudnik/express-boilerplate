var express                = require('express'),
    path                   = require('path'),
    logger                 = require('morgan'),
    cookieParser           = require('cookie-parser'),
    bodyParser             = require('body-parser'),
    compression            = require('compression'),
    cacheResponseDirective = require('express-cache-response-directive'),
    auth                   = require('http-auth'),
    fileStreamRotator      = require('file-stream-rotator');

var config = require('./config/config');

var basic = auth.basic({
    realm: "Private Area.",
    file : __dirname + "/config/users.txt"
});

var app = express();

var features;

try{
    features = app.locals.fts = require('./config/features.json');
}catch(e) {
    console.error('no config/features.json file');
}

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.disable('etag');
app.disable('x-powered-by');

if (app.get('env') !== 'production') {
    app.use(logger('dev'));
}

if(!config.disableAccessLog) {
    var accessLogStream      = fileStreamRotator.getStream({
        filename   : __dirname + "/logs/access-%DATE%.log",
        frequency  : "daily",
        verbose    : false,
        date_format: "YYYY-MM-DD"
    });
    var accessErrorLogStream = fileStreamRotator.getStream({
        filename   : __dirname + "/logs/access-error-%DATE%.log",
        frequency  : "daily",
        verbose    : false,
        date_format: "YYYY-MM-DD"
    });
    app.use(logger('combined', {stream: accessLogStream}));
    app.use(logger('combined', {
        stream: accessErrorLogStream,
        skip  : function (req, res) {
            return res.statusCode < 400
        }
    }));
}

app.use(auth.connect(basic));

if(config.webServerParams.compression) {
    app.use(compression({
        threshold: '4kb'
    }));
}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(require('static-asset')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public'), {
    etag  : false,
    maxAge: '7d'
}));

app.use(cacheResponseDirective());
app.use(function (req, res, next) {
    res.cacheControl({
        "no-cache"      : true,
        "no-store"      : true,
        "mustRevalidate": true
    });

    res.locals.baseURL = req.originalUrl;
    next();
});

app.use(function (req, res, next) {
    if (req.path.substr(-1) == '/' && req.path.length > 1) {
        var query = req.url.slice(req.path.length);
        res.redirect(301, req.path.slice(0, -1) + query);
    } else {
        next();
    }
});

app.use(function(req, res, next){
    req.features = typeof features === 'object' ? features : {};
    next();
});

app.use('/', require('./routes/index'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err    = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

if (app.get('env') === 'development') {
    /**
     * development error handler
     * will print stacktrace
     */
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error  : err
        });
    });
}

app.locals.fullInfo = config.fullinfo ? true : false;

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    var message = 'Wystąpił krytyczny błąd.';

    if (err.status == 404) {
        message = 'Not found';
    } else {
        res.use_express_redis_cache = false;
        console.log('[' + (new Date()).toUTCString() + '] [application error] [' + err + '] [' + req.url + ']');
    }

    res.status(err.status || 500);
    res.render('error', {
        message: message,
        error  : {}
    });
});


module.exports = app;

if (app.get('env') === 'production') {
    //warmUp();
}

if(!config.disableExpressRedisCache && config.flushCacheOnStartup) {
    require('./modules/cache').del('*', function (err, delNumber) {
        if (err) {
            console.log('error during cache flush: ' + err);
        } else {
            console.log('flushed cache, deleted ' + delNumber + ' entries');
        }
    })
}
