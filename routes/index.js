"use strict";
var express              = require('express'),
    router               = express.Router(),
    expressCacheOnDemand = require('express-cache-on-demand'),
    redisCache           = require('../modules/cache');

router.get('/',
    redisCache.route(),
    expressCacheOnDemand(),
    function (req, res) {
        res.render('index');
    });

module.exports = router;
